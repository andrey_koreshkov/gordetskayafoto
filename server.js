var http = require('http'),
	express = require('express'),
	app = express();

app.use(express.static(__dirname + "/public"));

app.get('/', (req, res) => {
	res.sendFile(__dirname + '/src/client/views/index.htm');
});

var server = http.Server(app);

server.listen(8081, () => {
	console.log('server listened on 8080 port...');
})