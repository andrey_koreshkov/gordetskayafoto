import React from 'react';

export class Navbar extends React.Component {
	constructor(props) {
		super(props);
		this.hashHandler = props.hashHandler;
		this.state = {
			active: props.active
		}
	}

	componentWillReceiveProps(props) {
		this.state.active = props.active;
	}

	render() {
		return (
			<div className="container">
				<nav className="navbar navbar-default">
					<div className="container">
						<div className="navbar-header">
							<button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
								<span className="sr-only">Toggle navigation</span>
								<span className="icon-bar"></span>
								<span className="icon-bar"></span>
								<span className="icon-bar"></span>
							</button>
							<a className="navbar-brand" href='#' onClick={this.hashHandler.bind(this, '')}>Irina Gordetska</a>
						</div>
						<div id="navbar" className="navbar-collapse collapse">
							<ul className="nav navbar-nav">
								<li className={this.state.active === 'main' ? 'active' : ''}><a href='#' onClick={this.hashHandler.bind(this, 'main')}>ГЛАВНАЯ</a></li>
								<li className={this.state.active === 'portfolio' ? 'active' : ''}><a href onClick={this.hashHandler.bind(this, 'portfolio')}>ПОРТФОЛИО</a></li>
								<li className={this.state.active === 'blog' ? 'active' : ''}><a href onClick={this.hashHandler.bind(this, 'blog')}>БЛОГ</a></li>
								<li className={this.state.active === 'service' ? 'active' : ''}><a href onClick={this.hashHandler.bind(this, 'service')}>УСЛУГИ</a></li>
								<li className={this.state.active === 'about' ? 'active' : ''}><a href onClick={this.hashHandler.bind(this, 'about')}>ОТЗЫВЫ</a></li>
								<li className={this.state.active === 'contact' ? 'active' : ''}><a href onClick={this.hashHandler.bind(this, 'contact')}>КОНТАКТЫ</a></li>
							</ul>
						</div>
					</div>
				</nav>
			</div>
		);
	}
};
