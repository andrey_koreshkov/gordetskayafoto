import React from 'react';

export class OrderButton extends React.Component {
	constructor(props) {
		super(props);
		this.hashHandler = props.hashHandler;
	}

	render() {
		return (
			<div className="formControl">
				<button className="btn btn-flat" onClick={this.hashHandler.bind(this, 'order')}>ОСТАВИТЬ ЗАЯВКУ</button>
			</div>
		);
	}
};
