import React from 'react';

export class Carousel extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div id="myCarousel" className="carousel slide col-xs-offset-3 col-xs-7" style={{margin: 0}} data-ride="carousel">
				<ol className="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" className=""></li>
					<li data-target="#myCarousel" data-slide-to="1" className="active"></li>
					<li data-target="#myCarousel" data-slide-to="2" className=""></li>
				</ol>
				<div className="carousel-inner" role="listbox">
					<div className="item">
						<img className="first-slide" src="/img/1.jpg" alt="First slide"></img>
					</div>
					<div className="item active">
						<img className="second-slide" src="/img/2.jpg" alt="Second slide"></img>
					</div>
					<div className="item">
						<img className="third-slide" src="/img/3.jpg" alt="Third slide"></img>
					</div>
				</div>
				<a className="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
					<span className="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span className="sr-only">Previous</span>
				</a>
				<a className="right carousel-control" href="#myCarousel" role="button" data-slide="next">
					<span className="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span className="sr-only">Next</span>
				</a>
			</div>
		);
	}
};
