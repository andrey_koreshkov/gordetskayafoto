require('babel/polyfill');

import React from 'react';
import ReactDom from 'react-dom';
import { Navbar } from './components/Navbar';
import { Carousel } from './components/Carousel';
import { SocialBar } from './components/SocialBar';
import { OrderButton } from './components/OrderButton';
import { Order } from './components/Order';
import { Portfolio } from './components/menu/Portfolio';
import { Blog } from './components/menu/Blog';
import { Service } from './components/menu/Service';
import { About } from './components/menu/About';
import { Contact } from './components/menu/Contact';

export default class Main extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			active: 'main',
			reload: true
		};
		this.switchDirective = () => {
			switch (this.state.active) {
				case 'order':
					return <Order/>;
				case 'main':
					return <Carousel/>;
				case 'portfolio':
					return <Portfolio/>;
				case 'blog':
					return <Blog/>;
				case 'service':
					return <Service/>;
				case 'about':
					return <About/>;
				case 'contact':
					return <Contact/>;
				default:
					return <Carousel/>;
			}
		};
		this.hashHandler = (hash, e) => {
			if (e) e.preventDefault();
			location.hash = this.state.reload ? location.hash : `#${hash && hash.length ? hash : ''}`;
			this.setState({
				active: location.hash && location.hash.length ? location.hash.slice(1, location.hash.length) : 'main'
			});
		}
	}

	componentWillMount() {
		if (this.state.reload) {
			this.hashHandler();
			this.state.reload = false;
		};
	}

	render() {
		return (
			<div className="container-fluid">
				<Navbar hashHandler={this.hashHandler} active={this.state.active}/>
				<div className="row">
					<div className="col-xs-offset-1 col-xs-2">
						<OrderButton hashHandler={this.hashHandler}/>
						<SocialBar/>
					</div>
					{this.switchDirective()}
				</div>
				<strong className="col-xs-offset-3"><i>Данный ресурс находится в разработке! Заявленный функционал будет добавляться по мере развития!</i></strong>
			</div>
		);
	}
};

ReactDom.render( < Main / > , document.getElementById('mainholder'));
